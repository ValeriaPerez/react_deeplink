# ReactJS - Aeroméxico

### Clonamos repositorio de [Bitbucket](https://bitbucket.org/ValeriaPerez/react_deeplink/src/master/)

~~~ 
  $ git clone git clone git@bitbucket.org:ValeriaPerez/react_deeplink.git
~~~
  ó
~~~ Git
  $ git clone git clone https://ValeriaPerez@bitbucket.org/ValeriaPerez/react_deeplink.git
~~~

### Accedemos a nuestro repositorio

~~~ 
  $ cd React_DeepLink
~~~

### Intalamos dependencias
~~~ 
  $ npm install
~~~
  
### Run

~~~ Git
  $ npm run serve
~~~


### En nuestro navegador vamos a:

  * [http://localhost:3000/](http://localhost:3000/)


________________________________________________________________________________________

##### Frontend developer [Valeria Bitbucket](https://bitbucket.org/ValeriaPerez/) - [Valeria GitHub](https://github.com/ValeriaPerez)
    



