import axios from 'axios';

export function getCatalogue() {
  return {
    type: 'GET_CATALOGUE',
    payload: axios.get('/app/data.json')
  };
}
