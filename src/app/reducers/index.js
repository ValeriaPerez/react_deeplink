import {combineReducers} from 'redux';
import todos from './todos';
import catalogos from './catalogos';

const rootReducer = combineReducers({
  todos,
  catalogos
});

export default rootReducer;
