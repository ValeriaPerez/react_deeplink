const initialState = {
  vehiculos: [],
  loading: false,
  error: ''
};

export default function catalogos(state = initialState, action) {
  switch (action.type) {
    case 'GET_CATALOGUE_PENDING':
      return {
        ...state,
        loading: true
      };
    case 'GET_CATALOGUE_REJECTED':
      return {
        ...state,
        loading: false,
        error: 'Hubo un error'
      };
    case 'GET_CATALOGUE_FULFILLED':
      return {
        ...state,
        vehiculos: action.payload.data.results,
        loading: false
      };
    default:
      return state;
  }
}
