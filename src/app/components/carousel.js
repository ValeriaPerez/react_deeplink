import React from 'react';
import PropTypes from 'prop-types';

import { Carousel } from 'antd';
import CardItem from './card-horizontal';

import data from '../best-seller.json';

const CarouselItems = props => (
	<Carousel autoplay>
		{data.options._collection.map((item, index) => {
          return (
            <CardItem
              key={index}
              deeplink={item.deepLink}
              image={item.image}
              alt={item.name}
              name={item.name}
              dailyPrice={item.dailyPrice}
              price={item.price}
              currency={item.currency}
              transmission={item.features.transmission}
              doors={item.features.doors}
              seats={item.features.seats}
              group={item.features.group}
              subGroup={item.features.subGroup}
              airco={item.features.airco}
              rate={item.features.rate}
            />
          );
        })}
	</Carousel>
);

CarouselItems.propTypes = {
};

CarouselItems.defaultProps = {
  
};

export default CarouselItems;
