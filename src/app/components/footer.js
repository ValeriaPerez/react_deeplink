import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'antd';

const FooterText = props => (
  <div className="footer__container">
  	<h5 className="footer__text">Valeria Pérez Jaimes - Resct JS</h5>
  	<div className="footer__icons">
  		<a title="Perfil Valeria Pérez" href="https://github.com/ValeriaPerez" target="_blank">
  			<Icon className="footer__icon" type="github" />
  		</a>
  		<a title="Perfil Valeria Pérez" href="https://www.linkedin.com/in/vpjaimes/" target="_blank">
  			<Icon className="footer__icon" type="linkedin" />
  		</a>
  	</div>
  </div>
);

FooterText.propTypes = {
};

FooterText.defaultProps = {
  
};

export default FooterText;
