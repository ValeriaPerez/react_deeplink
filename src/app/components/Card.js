import React from 'react';
import PropTypes from 'prop-types';
import { Card, Avatar, Rate } from 'antd';

const { Meta } = Card;

const Item = props => (
	<Card
   		className="card-item"
	    cover={
	    	<a 
	    		href={props.deeplink}
	    		title={props.alt}
	    		className="card-item__image"
	    		style={{ backgroundImage:(`url(${props.image})`)}}>
	    	</a>}>
	    <Meta
	      avatar={<Avatar src="https://thumb.lovemondays.com.br/image/5bf7a23c94134c409ea27cc94aca555b/logos/f3827d/aeromexico.png" />}
	      title={props.name}
	      description={props.dailyPrice}
	    />
	    <Rate allowHalf defaultValue={props.rate} className="card-item__rate"/>
	    <div className="card-item__features">
	    	<p className="card-item__features__text">Precio: <strong className="green">{props.price} <span>{props.currency}</span></strong></p>
	    	<p className="card-item__features__text">Transmision: <strong>{props.transmission}</strong></p>
	    	<p className="card-item__features__text">Puertas: <strong>{props.doors}</strong></p>
	    	<p className="card-item__features__text">Asientos: <strong>{props.seats}</strong></p>
	    	<p className="card-item__features__text">Grupo: <strong>{props.subGroup}</strong></p>
	    	<p className="card-item__features__text">Aire Acondicionado: <strong>{props.airco ? 'Si' : 'No'}</strong></p>
	    	<p><strong>{props.group}</strong></p>
	    </div>
	</Card>
);

Item.propTypes = {
	image: PropTypes.string,
	alt: PropTypes.string,
	name: PropTypes.string,
	price: PropTypes.number,
	deeplink: PropTypes.string,
	dailyPrice: PropTypes.number,
	currency: PropTypes.string,
	transmission: PropTypes.string,
	group: PropTypes.string,
	subGroup: PropTypes.string,
	airco: PropTypes.bool,
	rate: PropTypes.number,
};

Item.defaultProps = {
	image: 'https://thumb.lovemondays.com.br/image/5bf7a23c94134c409ea27cc94aca555b/logos/f3827d/aeromexico.png',
	alt: 'Imagen no encontrada',
	name: 'Nombre',
	price: 'sin precio',
	deeplink: 'https://autos.aeromexico.com/es-mx/',
	dailyPrice: 0,
	currency: 'NA',
	transmission: 'NA',
	doors: 0,
	seats: 0,
	group: 'NA',
	subGroup: 'NA',
	rate: 1,
};

export default Item;
