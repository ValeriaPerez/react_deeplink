import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, Icon } from 'antd';

const HeaderText = props => (
  <div className="header">
  	<Avatar className="header__logo" src="https://thumb.lovemondays.com.br/image/5bf7a23c94134c409ea27cc94aca555b/logos/f3827d/aeromexico.png" />
  	<h5 className="header__title">CATALOGO</h5>
  	<a className="header__icon" title="Descargar CV" href="../styles/Valeria_CV.pdf" download>
		<Icon type="download" />
	</a>
  </div>
);

HeaderText.propTypes = {
};

HeaderText.defaultProps = {
  
};

export default HeaderText;
