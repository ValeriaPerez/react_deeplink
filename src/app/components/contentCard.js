
import React from 'react';
import PropTypes from 'prop-types';
import CardItem from '../components/card';

const HeaderText = props => (
    <div className="content">
        {data.options._collection.map((item, index) => {
            return (
                <CardItem
                    key={index}
                    loading={this.state.loading}
                    deeplink={item.deepLink}
                    image={item.image}
                    alt={item.name}
                    name={item.name}
                    dailyPrice={item.dailyPrice}
                    price={item.price}
                    currency={item.currency}
                    transmission={item.features.transmission}
                    doors={item.features.doors}
                    seats={item.features.seats}
                    group={item.features.group}
                    subGroup={item.features.subGroup}
                    airco={item.features.airco}
                    rate={item.features.rate}
                />
            );
        })}
    </div>
);

HeaderText.propTypes = {
};

HeaderText.defaultProps = {
  
};

export default HeaderText;