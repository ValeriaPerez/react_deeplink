import React from 'react';
import PropTypes from 'prop-types';

import { Collapse } from 'antd';
import CardItem from './card';
import data from '../data.json';

const Panel = Collapse.Panel;

const DropContent = props => (
  <Collapse accordion>
    <Panel header="De lujo" key="1">
      { data.options._collection.price > 200 ? data.options._collection.map((item, index) => {
        return (
          <div key={index}>
            <CardItem 
              deeplink={item.deepLink}
              image={item.image}
              alt={item.name}
              name={item.name}
              dailyPrice={item.dailyPrice}
              price={item.price}
              currency={item.currency}
              transmission={item.transmission}
              doors={item.doors}
              seats={item.seats}
              subGroup={item.subGroup}
            />
          </div>
        );
      }) : <p>No se encontraron resultados</p>}
    </Panel>
    <Panel header="This is panel header 2" key="2">
      
    </Panel>
    <Panel header="This is panel header 3" key="3">
      
    </Panel>
  </Collapse>
);

DropContent.propTypes = {
  
};

DropContent.defaultProps = {
  
};

export default DropContent;