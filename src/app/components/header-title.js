import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, Icon } from 'antd';

const HeaderTitle = props => (
  <div className="content">
  	<h3 className="content__title">{props.title}</h3><Icon className="content__icon" type={props.type} />
  </div>
);

HeaderTitle.propTypes = {
	title: PropTypes.string,
	type: PropTypes.string,
};

HeaderTitle.defaultProps = {
	title: 'Titulo',
	type: 'question',
};

export default HeaderTitle;
