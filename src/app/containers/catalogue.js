import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';

import axios from 'axios';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../actions/index';

import data from '../data.json';
import { Layout, Icon, BackTop, Tabs } from 'antd';
import { StickyContainer, Sticky } from 'react-sticky';
import Button from 'antd/lib/button';
import HeadetText from '../components/header';
import CardItem from '../components/card';
import DropCatalogue from '../components/drop';
import FooterText from '../components/footer';
import CarouselItems from '../components/carousel';
import HeaderTitle from '../components/Header-title';

const { Header, Footer, Content } = Layout;

class Catalogue extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  //  puede ser para poner un loading
  componentWillMount() {
  }
  // ejecuta el metodo cuando el componente termino de montarse, cuando quiero cargar un catalogo usamos este
  componentDidMount() {
  }

  render() {

    return (
      <Layout>
        <Header>
          <HeadetText />
        </Header>
        <Content>
          <HeaderTitle title="Mejor Vendido" type="trophy" />
          <CarouselItems />
          <HeaderTitle title="Todos" type="appstore" />
          <div className="content">
            {data.options._collection.map((item, index) => {
              return (
                <CardItem
                  key={index}
                  loading={this.state.loading}
                  deeplink={item.deepLink}
                  image={item.image}
                  alt={item.name}
                  name={item.name}
                  dailyPrice={item.dailyPrice}
                  price={item.price}
                  currency={item.currency}
                  transmission={item.features.transmission}
                  doors={item.features.doors}
                  seats={item.features.seats}
                  group={item.features.group}
                  subGroup={item.features.subGroup}
                  airco={item.features.airco}
                  rate={item.features.rate}
                />
              );
            })}
          </div>
        </Content>
        <Footer className="footer">
          <FooterText />
        </Footer>
        <BackTop visibilityHeight="1000"/>
      </Layout>
    );
  }
}

Catalogue.propTypes = {
  catalogos: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    catalogos: state.catalogos
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Catalogue);
