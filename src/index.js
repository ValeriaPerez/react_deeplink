import 'babel-polyfill';

import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import Catalogue from './app/containers/catalogue';
import configureStore from './app/store/configureStore';
import {Router, Route, browserHistory} from 'react-router';

import 'antd/dist/antd.css';  // or 'antd/dist/antd.less'
import './app/styles/main.scss';

const store = configureStore();

render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={Catalogue}/>
    </Router>
  </Provider>,
  document.getElementById('root')
);
